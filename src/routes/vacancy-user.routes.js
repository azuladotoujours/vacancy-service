const { Router } = require('express');
const router = Router();
const { requireUserSignIn } = require('../helpers/vacancy.helper');
const {
  addCandidateToVacancy,
  removeCandidateToVacancy,
} = require('../controllers/vacancy-user.controller');

router.put(
  '/:vacancyId/addcandidate',
  requireUserSignIn,
  addCandidateToVacancy
);
router.put(
  '/:vacancyId/removecandidate',
  requireUserSignIn,
  removeCandidateToVacancy
);
module.exports = router;
