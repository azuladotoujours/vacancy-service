const { Router } = require('express');
const router = Router();
const {
  postVacancy,
  editVacancy,
  deleteVacancy,
  hireCandidate,
} = require('../controllers/vacancy-restaurant.controller');
const {
  requireRestaurantSignIn,
  hasRestaurantAuthorization,
} = require('../helpers/vacancy.helper');
const {
  vacancyValidator,
  vacancyValidations,
  checkDates,
  checkBalance,
  checkBalanceAtUpdateVacancy,
  hireCandidateValidator,
} = require('../helpers/vacancy-restaurant.validator');

router.put(
  '/:vacancyId/hire',
  requireRestaurantSignIn,
  hasRestaurantAuthorization,
  hireCandidateValidator,
  hireCandidate
);

router.post(
  '/newVacancy',
  requireRestaurantSignIn,
  vacancyValidations,
  vacancyValidator,
  checkDates,
  checkBalance,
  postVacancy
);
router.put(
  '/:vacancyId',
  requireRestaurantSignIn,
  hasRestaurantAuthorization,
  vacancyValidations,
  vacancyValidator,
  checkDates,
  checkBalanceAtUpdateVacancy,
  editVacancy
);

router.delete(
  '/:vacancyId',
  requireRestaurantSignIn,
  hasRestaurantAuthorization,
  deleteVacancy
);

module.exports = router;
