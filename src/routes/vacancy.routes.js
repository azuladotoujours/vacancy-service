const { Router } = require('express');
const router = Router();
const {
  getVacancies,
  getVacancy,
} = require('../controllers/vacancy.controller');

router.get('/', getVacancies);
router.get('/:vacancyId', getVacancy);

module.exports = router;
