const dbQuery = require('../config');

const addCandidateToVacancy = async (req, res) => {
  const vacancyId = req.params.vacancyId;
  const userId = req.authUser.userId;

  const active = await checkVacancyStatus(vacancyId);

  if (!active) {
    return res
      .status(400)
      .json({ error: 'The vacancy does not exists or is expired' });
  }

  const userInVacancy = await checkUserInVacancy(userId, vacancyId);

  if (userInVacancy) {
    return res.status(400).json({ error: 'The user is already a candidate' });
  }

  const { addCandidateQuery } = require('../helpers/vacancy.querys');

  values = [vacancyId, userId];

  const { getRestaurantIdQuery } = require('../helpers/vacancy.querys');

  const restaurantId = await dbQuery.query(getRestaurantIdQuery, [vacancyId]);

  const { fetchRestaurant } = require('./vacancy-restaurant.controller');
  const restaurant = await fetchRestaurant(
    restaurantId.rows[0].restaurant_id,
    res
  );
  let restaurantMail = restaurant.data.restaurant.email;
  const { newCandidatMail } = require('../helpers/vacancy-mailer');

  try {
    const success = await dbQuery.query(addCandidateQuery, values);
    await newCandidatMail(restaurantMail);
    return res.status(200).json({ message: 'Applied successfuly!' });
  } catch (e) {
    console.log(e);
  }
};

const removeCandidateToVacancy = async (req, res) => {
  const vacancyId = req.params.vacancyId;
  const userId = req.authUser.userId;

  const active = await checkVacancyStatus(vacancyId);

  if (!active) {
    return res
      .status(400)
      .json({ error: 'The vacancy does not exists or is expired' });
  }

  const userInVacancy = await checkUserInVacancy(userId, vacancyId);

  if (!userInVacancy) {
    return res
      .status(400)
      .json({ error: 'The user has not applied to the vacancy' });
  }

  const { removeCandidateQuery } = require('../helpers/vacancy.querys');

  values = [vacancyId, userId];

  try {
    const success = await dbQuery.query(removeCandidateQuery, values);

    return res.status(200).json({ message: 'Retired successfuly!' });
  } catch (e) {
    console.log(e);
  }
};

const checkVacancyStatus = async (vacancyId) => {
  const { getVacancyQuery } = require('../helpers/vacancy.querys');

  const vacancy = await dbQuery.query(getVacancyQuery, [vacancyId]);

  if (!vacancy.rows[0] || vacancy.rows[0].status != 'ACTIVE') {
    return false;
  }

  return true;
};

const checkUserInVacancy = async (userId, vacancyId) => {
  const { searchCandidateQuery } = require('../helpers/vacancy.querys');

  const refactorUserId = `{${userId}}`;

  values = [vacancyId, refactorUserId];

  const candidateInVacancy = await dbQuery.query(searchCandidateQuery, values);

  if (candidateInVacancy.rows[0]) {
    return true;
  }

  return false;
};

module.exports = {
  addCandidateToVacancy,
  removeCandidateToVacancy,
  checkUserInVacancy,
  checkVacancyStatus,
};
