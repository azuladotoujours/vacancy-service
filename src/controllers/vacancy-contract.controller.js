const dbQuery = require('../config');
const { getVacancyQuery } = require('../helpers/vacancy.querys');

const createContract = async (vacancyId, res) => {
  try {
    const vacancyRows = await dbQuery.query(getVacancyQuery, [vacancyId]);
    const vacancy = vacancyRows.rows[0];
    console.log(vacancy);
  } catch (e) {
    console.log(e);
    res.status(200).json({ error: 'En el contrato' });
  }
};

module.exports = { createContract };
