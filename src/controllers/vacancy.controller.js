const dbQuery = require('../config');
const moment = require('moment');

const getVacancies = async (req, res) => {
  try {
    const { getAllVacanciesQuery } = require('../helpers/vacancy.querys');

    const vacancies = await dbQuery.query(getAllVacanciesQuery);

    const parsedVacancies = parseDates(vacancies.rows);

    res.status(200).json(parsedVacancies);
  } catch (e) {
    res.status(500).send(e);
  }
};

const getVacancy = async (req, res) => {
  const vacancyId = req.params.vacancyId;

  const { getVacancyQuery } = require('../helpers/vacancy.querys');

  const vacancy = await dbQuery.query(getVacancyQuery, [vacancyId]);

  if (!vacancy.rows[0]) {
    return res.status(400).json({ error: 'No Vacancy with that id' });
  }

  const parsedVacancy = parseDates(vacancy.rows);

  return res.status(200).json(parsedVacancy);
};

parseDates = (vacancies) => {
  for (let i = 0; i < vacancies.length; i++) {
    vacancies[i].start_at = moment(vacancies[i].start_at)
      .subtract(5, 'hours')
      .utc()
      .format();
    vacancies[i].end_at = moment(vacancies[i].end_at)
      .subtract(5, 'hours')
      .utc()
      .format();
    vacancies[i].created_at = moment(vacancies[i].created_at)
      .subtract(5, 'hours')
      .utc()
      .format();
    if (vacancies[i].updated_at) {
      vacancies[i].updated_at = moment(vacancies[i].updated_at)
        .subtract(5, 'hours')
        .utc()
        .format();
    }
  }

  return vacancies;
};

module.exports = {
  getVacancies,
  getVacancy,
};
