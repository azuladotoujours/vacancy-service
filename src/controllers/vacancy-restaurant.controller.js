const dbQuery = require('../config');
const moment = require('moment-timezone');
const axios = require('axios');

const postVacancy = async (req, res) => {
  const {
    description,
    start_at,
    end_at,
    offers_quantity,
    payment_per_hour,
    country,
    state,
    city,
    address,
    position,
  } = req.body;

  const restaurantId = req.authRestaurant.restaurantId;

  const { createVacancyQuery } = require('../helpers/vacancy.querys');

  const created_at = moment(new Date()).tz('America/Bogota').format();

  const candidates = {};
  const hireds = {};

  const values = [
    restaurantId,
    description,
    start_at,
    end_at,
    offers_quantity,
    payment_per_hour,
    country,
    state,
    city,
    address,
    candidates,
    hireds,
    created_at,
    position,
  ];
  try {
    await dbQuery.query(createVacancyQuery, values);
    res.status(200).json({
      message: 'Vacancy posted succesfully',
      vacancyCost: req.params.vacancyCost,
    });
  } catch (e) {
    console.log(e);
    res.status(500).json({ error: 'Something went wrong' });
  }
};

const editVacancy = async (req, res) => {
  const updated_at = moment(new Date()).tz('America/Bogota').format();

  const vacancyId = req.params.vacancyId;

  const candidates = {};
  const hireds = {};

  const values = [
    vacancyId,
    req.body.description,
    req.body.start_at,
    req.body.end_at,
    req.body.offers_quantity,
    req.body.payment_per_hour,
    req.body.country,
    req.body.state,
    req.body.city,
    req.body.address,
    updated_at,
    req.body.position,
    candidates,
    hireds,
  ];

  const { updateVacancyQuery } = require('../helpers/vacancy.querys');
  const {
    notifyCandidatesAndHireds,
  } = require('../helpers/vacancy-user.helper');
  try {
    notifyCandidatesAndHireds(vacancyId);

    await dbQuery.query(updateVacancyQuery, values);

    res.status(200).json({
      message: 'Vacancy updated succesfully',
      vacancyCost: req.params.vacancyCost,
    });
  } catch (e) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};

const deleteVacancy = async (req, res) => {
  try {
    const vacancyId = req.params.vacancyId;
    const { deleteVacancyQuery } = require('../helpers/vacancy.querys');
    const valuesAtVacancy = await dbQuery.query(deleteVacancyQuery, [
      vacancyId,
    ]);
    const {
      deleteVacancyAndReturnMoney,
    } = require('../helpers/vacancy-restaurant.validator');
    await deleteVacancyAndReturnMoney(valuesAtVacancy.rows[0], req);
    res.status(200).json({
      message: 'Vacancy deleted succesfully',
      vacancyCost: req.params.vacancyCost,
    });
  } catch (e) {
    return res.status(500).json({ error: 'Something went wrong' });
  }
};

const hireCandidate = async (req, res) => {
  const {
    hireCandidateQuery,
    removeCandidateQuery,
  } = require('../helpers/vacancy.querys');

  try {
    //Hire the candidate

    let values = [req.params.vacancyId, req.body.userId];

    await dbQuery.query(hireCandidateQuery, values);

    //Erase the candidate from candidates

    await dbQuery.query(removeCandidateQuery, values);

    if (req.createContract) {
      const { notifyHireds } = require('../helpers/vacancy-user.helper');
      const { changeVacancyStatus } = require('../helpers/vacancy.helper');
      const { createContract } = require('./vacancy-contract.controller');
      notifyHireds(req.params.vacancyId);
      await createContract(req.params.vacancyId, res);
      await changeVacancyStatus(req.params.vacancyId);

      return res.status(200).json({
        message:
          'Candidato escogido exitosamente, en este momento se procede a notificar a los contratados, cerrar la vacante y crear el contrato.',
      });
    } else {
      return res.status(200).json({
        message: 'Candidato escogido exitosamente.',
      });
    }
  } catch (e) {
    console.log(e);
    return res.status(200).json({ error: 'Error al escoger candidato' });
  }
};

const fetchRestaurant = async (restaurantId, res) => {
  try {
    const response = await axios.get(
      `${process.env.RESTAURANT_HOST}/${restaurantId}`
    );
    return response;
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: 'Error fetching restaurant' });
  }
};

module.exports = {
  postVacancy,
  editVacancy,
  deleteVacancy,
  fetchRestaurant,
  hireCandidate,
};
