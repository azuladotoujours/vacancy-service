const nodemailer = require('nodemailer');
require('dotenv').config();

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    type: 'OAuth2',
    user: process.env.GMAIL_EMAIL,
    clientId: process.env.GMAIL_CLIENT_ID,
    clientSecret: process.env.GMAIL_SECRET,
    refreshToken: process.env.GMAIL_REFRESH_TOKEN,
    accessToken: process.env.GMAIL_ACCESS_TOKEN,
  },
});

const newCandidatMail = async (email) => {
  const mailOptions = {
    from: `${process.env.GMAIL_EMAIL}`,
    to: `${email}`,
    subject: 'Nuevo Candidato!',
    html: `<p>Tienes un nuevo candidato en una de tus ofertas, ve a verlo!</p>`,
  };

  await transporter.sendMail(mailOptions, function (err, success) {
    if (err) {
      console.log(err);
    } else {
      console.log(success);
    }
  });
};

const notifyCandidatesAndHiredsMail = async (vacancyId, emails) => {
  const mailOptions = {
    from: `${process.env.GMAIL_EMAIL}`,
    to: emails,
    subject: 'Cambios en la vacante',
    html: `<p>Han habido cambios en una de las vacantes a la cuál estás inscrito. Te invitamos a ver sí la vacante aún se acomoda a tus necesidades en el siguiente link: ${process.env.CLIENT_HOST}/v1/vacancy/${vacancyId}</p>`,
  };

  await transporter.sendMail(mailOptions, function (err, success) {
    if (err) {
      console.log(err);
    } else {
      console.log(success);
    }
  });
};

const notifyHiredsMail = async (emails) => {
  const mailOptions = {
    from: `${process.env.GMAIL_EMAIL}`,
    to: emails,
    subject: 'Contratado!',
    html: `<p>Usted ha sido contratado! Logeate y ve a ver la información de tu contrato en el siguiente link ${process.env.CLIENT_HOST}/v1/vacancy/login</p>`,
  };

  await transporter.sendMail(mailOptions, function (err, success) {
    if (err) {
      console.log(err);
    } else {
      console.log(success);
    }
  });
};

module.exports = {
  newCandidatMail,
  notifyCandidatesAndHiredsMail,
  notifyHiredsMail,
};
