const expressJwt = require('express-jwt');
const dotenv = require('dotenv');
const dbQuery = require('../config');
dotenv.config();

const requireRestaurantSignIn = expressJwt({
  //if the token is valid, express jwt appends the verified admin id
  //in an auth key to the request object
  secret: process.env.RESTAURANT_SECRET,
  userProperty: 'authRestaurant',
});

const requireUserSignIn = expressJwt({
  //if the token is valid, express jwt appends the verified admin id
  //in an auth key to the request object
  secret: process.env.USER_SECRET,
  userProperty: 'authUser',
});

const hasRestaurantAuthorization = async (req, res, next) => {
  try {
    const vacancyId = req.params.vacancyId;

    const { getVacancyQuery } = require('../helpers/vacancy.querys');
    const vacancy = await dbQuery.query(getVacancyQuery, [vacancyId]);
    if (!vacancy.rows[0]) {
      return res.status(200).json({ error: 'No Vacancy with that id' });
    }

    const hasAuthorization =
      req.authRestaurant.restaurantId === vacancy.rows[0].restaurant_id;

    if (!hasAuthorization) {
      return res
        .status(401)
        .json({ error: 'You are not authorized to perform that action' });
    }

    next();
  } catch (e) {
    return res.status(500).json({ error: 'Wrong id format ' });
  }
};

const changeVacancyStatus = async (vacancyId) => {
  const { changeVacancyStatus } = require('./vacancy.querys');

  await dbQuery.query(changeVacancyStatus, [vacancyId]);
};

module.exports = {
  requireRestaurantSignIn,
  requireUserSignIn,
  hasRestaurantAuthorization,
  changeVacancyStatus,
};
