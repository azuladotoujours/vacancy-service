exports.createVacancyQuery =
  'INSERT INTO vacancies (restaurant_id, description, start_at, end_at, offers_quantity, payment_per_hour, country, state, city, address, candidates, hireds, created_at, position_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)';

exports.updateVacancyQuery =
  'UPDATE vacancies SET description = $2, start_at = $3, end_at = $4, offers_quantity = $5, payment_per_hour = $6, country = $7, state = $8, city = $9, address = $10, updated_at = $11, position_id = $12, candidates = $13, hireds = $14   WHERE id = $1 ';

exports.deleteVacancyQuery =
  'DELETE FROM vacancies where id = $1 RETURNING start_at, end_at, offers_quantity, payment_per_hour';

exports.getAllVacanciesQuery = 'SELECT * FROM vacancies';

exports.getVacancyQuery = 'SELECT * FROM vacancies where id = $1';

exports.addCandidateQuery =
  'UPDATE vacancies set candidates = array_append(candidates, $2) where id = $1';

exports.hireCandidateQuery =
  'UPDATE vacancies set hireds = array_append(hireds, $2) where id = $1';

exports.removeCandidateQuery =
  'UPDATE vacancies set candidates = array_remove(candidates, $2) where id = $1';

exports.searchCandidateQuery =
  'SELECT * FROM vacancies where id = $1 AND candidates @> $2';

exports.getRestaurantIdQuery =
  'SELECT restaurant_id FROM vacancies WHERE id = $1';

exports.summatoryHiredQuery =
  'SELECT array_length(hireds, 1) FROM vacancies WHERE  id = $1';

exports.getCandidates =
  'SELECT row_to_json(row) FROM (SELECT candidates FROM vacancies WHERE id = $1) row';

exports.getHireds =
  'SELECT row_to_json(row) FROM (SELECT hireds FROM vacancies WHERE id = $1) row';

exports.changeVacancyStatus =
  "UPDATE vacancies SET status = 'CLOSED' WHERE id = $1 ";
