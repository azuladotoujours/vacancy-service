const { check, validationResult } = require('express-validator');
const moment = require('moment');
const dbQuery = require('../config');
const {
  checkVacancyStatus,
} = require('../controllers/vacancy-user.controller');
const {
  searchCandidateQuery,
  summatoryHiredQuery,
} = require('../helpers/vacancy.querys');

exports.vacancyValidations = [
  check('description', 'You must add a description').notEmpty(),
  check('start_at', 'Enter a valid date to start the vacancy').isISO8601(),
  check('end_at', 'Enter a valid date to finish the vacancy').isISO8601(),
  check(
    'offers_quantity',
    'The offert must contain at least one vacancy'
  ).matches(/[1-9]/),
  check('payment_per_hour', 'The offert must contain a payment').matches(
    /[1-9]/
  ),
  check('country', 'Must be in Colombia').notEmpty(),
  check('state', 'State required').notEmpty(),
  check('city', 'City required').notEmpty(),
  check('address', 'Address required').notEmpty(),
  check('position', 'Must be a valid position').isIn(['1']),
];

exports.vacancyValidator = (req, res, next) => {
  //Check for error
  const errors = validationResult(req);
  //if error show the first one as they happend
  if (!errors.isEmpty()) {
    const extractedErrors = [];
    errors.array().map((err) => extractedErrors.push(err.msg));
    return res.status(200).json({ error: extractedErrors[0] });
  }

  next();
};

exports.checkDates = (req, res, next) => {
  let checkStart = checkStartAtValidDate(req.body.start_at);

  let checkVacancyDuration = checkTimeBetweenStartAndEnd(
    req.body.start_at,
    req.body.end_at
  );

  if (!checkStart) {
    return res.status(200).json({
      error:
        'La vacante debe empezar por lo menos 8 horas después de la hora actual',
    });
  } else if (!checkVacancyDuration) {
    return res.status(200).json({
      error: 'La vacante debe tener una duración de 1 hora como mínimo',
    });
  } else if (checkStart && checkVacancyDuration) {
    next();
  }
};

checkStartAtValidDate = (startAtDate) => {
  let actualMoment = moment().format();
  let date = moment(moment(startAtDate).format());
  let difference = moment.duration(date.diff(actualMoment)).asHours();
  if (8 > difference) {
    return false;
  }
  return true;
};

checkTimeBetweenStartAndEnd = (startAtDate, endAtDate) => {
  let startAt = moment(moment(startAtDate).format());
  let endAt = moment(moment(endAtDate).format());

  var vancancyDuration = moment.duration(endAt.diff(startAt)).asHours();

  if (vancancyDuration < 1) {
    return false;
  }
  return true;
};

exports.checkBalance = async (req, res, next) => {
  let operation = await doOperationOfVacancy(req);
  let restaurantBalance = parseInt(req.authRestaurant.restaurantBalance);
  const enoughBalance = checkEnoughBalance(operation, restaurantBalance, req);
  if (enoughBalance) {
    next();
  } else {
    return res.status(200).json({
      error: 'Fondos insuficientes, lo invitamos a recargar su cuenta.',
    });
  }
};

exports.checkBalanceAtUpdateVacancy = async (req, res, next) => {
  let vacancyId = req.params.vacancyId;
  let queryString =
    'SELECT start_at, end_at, offers_quantity, payment_per_hour FROM vacancies WHERE id = $1';
  let valuesAtVacancy = await dbQuery.query(queryString, [vacancyId]);
  let operationAtVacancy = await doOperationAtRows(valuesAtVacancy.rows[0]);
  let newOperation = await doOperationOfVacancy(req);
  console.log('new operation:', newOperation);
  console.log('old operation:', operationAtVacancy);

  if (newOperation > operationAtVacancy) {
    operation = parseInt(newOperation - operationAtVacancy);
    let restaurantBalance = parseInt(req.authRestaurant.restaurantBalance);
    const enoughBalance = checkEnoughBalance(operation, restaurantBalance, req);

    if (!enoughBalance) {
      return res.status(200).json({
        error: 'Fondos insuficientes, lo invitamos a recargar su cuenta.',
      });
    } else {
      next();
    }
  } else if (operationAtVacancy > newOperation) {
    const operation = operationAtVacancy - newOperation;
    req.params.vacancyCost = operation;
    next();
  } else {
    req.params.vacancyCost = 0;
    next();
  }
};

checkEnoughBalance = (operation, restaurantBalance, req) => {
  if (operation > restaurantBalance) {
    return false;
  }
  req.params.vacancyCost = -operation;
  return true;
};

exports.deleteVacancyAndReturnMoney = async (valuesAtVacancy, req) => {
  const operation = await doOperationAtRows(valuesAtVacancy);
  req.params.vacancyCost = operation;
};

exports.hireCandidateValidator = async (req, res, next) => {
  //Check values in the body
  if (!req.params.vacancyId || !req.body.userId) {
    res.status(200).json({ error: 'Faltan parámetros' });
    return;
  }
  try {
    //Check vacancy status
    const active = await checkVacancyStatus(req.params.vacancyId);

    if (!active) {
      return res
        .status(200)
        .json({ error: 'La vacante se ha cerrado, o no existe.' });
    }
  } catch (e) {
    console.log('status');
    console.log(e);
  }

  let refactorUserId = `{${req.body.userId}}`;

  let values = [req.params.vacancyId, refactorUserId];

  //Check if the user is in a candidate and get the vancacy
  const vacancy = await dbQuery.query(searchCandidateQuery, values);

  if (vacancy.rowCount == 0) {
    return res.status(200).json({ error: 'El usuario no es un candidato' });
  }

  let offersQuantity = vacancy.rows[0].offers_quantity;

  try {
    let hiredCandidatesNumber = await dbQuery.query(summatoryHiredQuery, [
      req.params.vacancyId,
    ]);
    if (hiredCandidatesNumber.rows[0].array_length == null) {
      hiredCandidatesNumber = 0;
    } else {
      hiredCandidatesNumber = hiredCandidatesNumber.rows[0].array_length;
    }
    //Validate if the restaurant can hire another candidate
    if (hiredCandidatesNumber < parseInt(offersQuantity)) {
      //The restaurant has hired the amount of offers.
      if (hiredCandidatesNumber + 1 == parseInt(offersQuantity)) {
        req.createContract = true;
      }
      next();
    } else {
      return res.status(200).json({
        error:
          'Ya ha contratado el máximo de candidatos posible, lo invitamos a actualizar la oferta para seguir contratando.',
      });
    }
  } catch (e) {
    console.log('summatory');
    console.log(e);
  }
};

doOperationOfVacancy = async (req) => {
  let startAt = moment(moment(req.body.start_at).format());
  let endAt = moment(moment(req.body.end_at).format());

  var vacancyDuration = parseInt(
    moment.duration(endAt.diff(startAt)).asHours()
  );
  let vacants = parseInt(req.body.offers_quantity);
  let paymentPerHour = parseInt(req.body.payment_per_hour);

  let operation = vacancyDuration * paymentPerHour * vacants;

  return operation;
};

doOperationAtRows = async (valuesAtVacancy) => {
  let startAt = moment(moment(valuesAtVacancy.start_at).format());
  let endAt = moment(moment(valuesAtVacancy.end_at).format());

  var vacancyDuration = parseInt(
    moment.duration(endAt.diff(startAt)).asHours()
  );
  let vacants = parseInt(valuesAtVacancy.offers_quantity);
  let paymentPerHour = parseInt(valuesAtVacancy.payment_per_hour);

  let operation = vacancyDuration * paymentPerHour * vacants;

  return operation;
};
