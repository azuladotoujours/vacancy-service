const dbQuery = require('../config');
const { getCandidates, getHireds } = require('./vacancy.querys');
const axios = require('axios');
const {
  notifyCandidatesAndHiredsMail,
  notifyHiredsMail,
} = require('./vacancy-mailer');

const notifyCandidatesAndHireds = async (vacancyId) => {
  const candidatesRows = await dbQuery.query(getCandidates, [vacancyId]);
  const hiredsIdRows = await dbQuery.query(getHireds, [vacancyId]);
  let candidatesId = candidatesRows.rows[0].row_to_json.candidates;
  let hiredsId = hiredsIdRows.rows[0].row_to_json.hireds;

  let idsArray = [...candidatesId, ...hiredsId];

  if (idsArray !== 'undefined' && idsArray.length > 0) {
    const fetch = idsArray.map((id) => {
      return axios
        .get(`${process.env.USER_HOST}/getemail/${id}`)
        .then((res) => res.data.email)
        .catch((e) => console.log(e));
    });

    Promise.all(fetch).then((res) => {
      notifyCandidatesMail(vacancyId, res);
    });
  }
};

const notifyHireds = async (vacancyId) => {
  const hiredsIdRows = await dbQuery.query(getHireds, [vacancyId]);
  let hiredsId = hiredsIdRows.rows[0].row_to_json.hireds;

  if (hiredsId !== 'undefined' && hiredsId.length > 0) {
    const fetch = hiredsId.map((id) => {
      return axios
        .get(`${process.env.USER_HOST}/getemail/${id}`)
        .then((res) => res.data.email)
        .catch((e) => console.log(e));
    });

    Promise.all(fetch).then((res) => {
      console.log(res);
      notifyHiredsMail(res);
    });
  }
};

module.exports = { notifyCandidatesAndHireds, notifyHireds };
